const mongoose = require('mongoose');

/* 
Activity 1:

    >> Create a User model out of the rough sketch made for our models.
    >> Follow the sketch and add the fields and the types.
        Note: All fields are required except for isAdmin.
        Note: isAdmin, 
              Date, 
              status = "Enrolled" 
              has a default value.
*/

let userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First name cannot be empty'],
    },
    lastName: {
        type: String,
        required: [true, 'Last name cannot be empty'],
    },
    email: {
        type: String,
        required: [true, 'Email cannot be empty'],
    },
    password: {
        type: String,
        required: [true, 'Password cannot be empty'],
    },
    mobileNo: {
        type: String,
        required: [true, 'Mobile number cannot be empty'],
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    enrollments: [{
        courseId: {
            type: String,
            required: [true, 'Couse Id cannot be empty'],
        },
        status: {
            type: String,
            default: 'Enrolled',
        },
        dateEnrolled: {
            type: Date,
            default: new Date(),
        },
    }, ],
});

module.exports = mongoose.model('User', userSchema);