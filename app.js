//NOTE: Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//NOTE: Server
const app = express();
const port = 4000;

//NOTE: Database Connection
mongoose.connect(
    'mongodb+srv://admin:admin@wdc028-course-booking.j89li.mongodb.net/course-booking?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error'));
db.once('open', () => console.log('Successfully connected to MongoDB'));

//NOTE: Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//NOTE: Group Routing

//NOTE: Port listener
app.listen(port, () => console.log(`Server is listening in port: ${port}`));